'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found

        $urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/variables/views/home-variable.client.view.html'
		})
        .state('otherwise', {
                url: '/',
                templateUrl: 'modules/variables/views/home-variable.client.view.html'
            });
	}
]);
