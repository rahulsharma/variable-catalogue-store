'use strict';

//Variables service used for communicating with the articles REST endpoints
angular.module('variables').factory('Variables', ['$resource',
    function($resource) {
        return $resource('variables/:variableId', {
            variableId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
