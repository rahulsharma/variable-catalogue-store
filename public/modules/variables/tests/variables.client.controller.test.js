'use strict';

(function() {
    // Variables Controller Spec
    describe('VariablesController', function() {
        // Initialize global variables
        var VariablesController,
            scope,
            $httpBackend,
            $stateParams,
            $location;

        // The $resource service augments the response object with methods for updating and deleting the resource.
        // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
        // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
        // When the toEqualData matcher compares two objects, it takes only object properties into
        // account and ignores methods.
        beforeEach(function() {
            jasmine.addMatchers({
                toEqualData: function(util, customEqualityTesters) {
                    return {
                        compare: function(actual, expected) {
                            return {
                                pass: angular.equals(actual, expected)
                            };
                        }
                    };
                }
            });
        });

        // Then we can start by loading the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));

        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
            // Set a new global scope
            scope = $rootScope.$new();

            // Point global variables to injected services
            $stateParams = _$stateParams_;
            $httpBackend = _$httpBackend_;
            $location = _$location_;

            // Initialize the Variables controller.
            VariablesController = $controller('VariablesController', {
                $scope: scope
            });
        }));

        it('$scope.find() should create an array with at least one variable object fetched from XHR', inject(function(Variables) {
            // Create sample variable using the Variables service
            var sampleVariable = new Variables({
                title: 'A Variable about SOMETHING',
                content: 'SOMETHING rocks!'
            });

            // Create a sample variables array that includes the new variable
            var sampleVariables = [sampleVariable];

            // Set GET response
            $httpBackend.expectGET('variables').respond(sampleVariables);

            // Run controller functionality
            scope.find();
            $httpBackend.flush();

            // Test scope value
            expect(scope.variables).toEqualData(sampleVariables);
        }));

        it('$scope.findOne() should create an array with one variable object fetched from XHR using a variableId URL parameter', inject(function(Variables) {
            // Define a sample variable object
            var sampleVariable = new Variables({
                title: 'A Variable about SOMETHING',
                content: 'SOMETHING rocks!'
            });

            // Set the URL parameter
            $stateParams.variableId = '525a8422f6d0f87f0e407a33';

            // Set GET response
            $httpBackend.expectGET(/variables\/([0-9a-fA-F]{24})$/).respond(sampleVariable);

            // Run controller functionality
            scope.findOne();
            $httpBackend.flush();

            // Test scope value
            expect(scope.variable).toEqualData(sampleVariable);
        }));

        it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Variables) {
            // Create a sample variable object
            var sampleVariablePostData = new Variables({
                title: 'A Variable about SOMETHING',
                content: 'SOMETHING rocks!'
            });

            // Create a sample variable response
            var sampleVariableResponse = new Variables({
                _id: '525cf20451979dea2c000001',
                title: 'A Variable about SOMETHING',
                content: 'SOMETHING rocks!'
            });

            // Fixture mock form input values
            scope.title = 'A Variable about SOMETHING';
            scope.content = 'SOMETHING rocks!';

            // Set POST response
            $httpBackend.expectPOST('variables', sampleVariablePostData).respond(sampleVariableResponse);

            // Run controller functionality
            scope.create();
            $httpBackend.flush();

            // Test form inputs are reset
            expect(scope.title).toEqual('');
            expect(scope.content).toEqual('');

            // Test URL redirection after the variable was created
            expect($location.path()).toBe('/variables/' + sampleVariableResponse._id);
        }));

        it('$scope.update() should update a valid variable', inject(function(Variables) {
            // Define a sample variable put data
            var sampleVariablePutData = new Variables({
                _id: '525cf20451979dea2c000001',
                title: 'A Variable about SOMETHING',
                content: 'SOMETHING rocks!'
            });

            // Mock variable in scope
            scope.variable = sampleVariablePutData;

            // Set PUT response
            $httpBackend.expectPUT(/variables\/([0-9a-fA-F]{24})$/).respond();

            // Run controller functionality
            scope.update();
            $httpBackend.flush();

            // Test URL location to new object
            expect($location.path()).toBe('/variables/' + sampleVariablePutData._id);
        }));

        it('$scope.remove() should send a DELETE request with a valid variableId and remove the variable from the scope', inject(function(Variables) {
            // Create new variable object
            var sampleVariable = new Variables({
                _id: '525a8422f6d0f87f0e407a33'
            });

            // Create new variables array and include the variable
            scope.variables = [sampleVariable];

            // Set expected DELETE response
            $httpBackend.expectDELETE(/variables\/([0-9a-fA-F]{24})$/).respond(204);

            // Run controller functionality
            scope.remove(sampleVariable);
            $httpBackend.flush();

            // Test array after successful delete
            expect(scope.variables.length).toBe(0);
        }));
    });
}());
