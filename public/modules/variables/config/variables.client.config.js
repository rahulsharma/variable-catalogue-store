'use strict';

// Configuring the Variables module
angular.module('variables').run(['Menus',
    function(Menus) {
        // Set top bar menu items
        Menus.addMenuItem('topbar', 'Variables', 'variables', 'dropdown', '/variables(/create)?');
        Menus.addSubMenuItem('topbar', 'variables', 'List Variables', 'variables');
        Menus.addSubMenuItem('topbar', 'variables', 'New Variable', 'variables/create');
    }
]);
