'use strict';

// Setting up route
angular.module('variables').config(['$stateProvider',
    function($stateProvider) {
        // Variables state routing
        $stateProvider.
            state('listVariables', {
                url: '/variables',
                templateUrl: 'modules/variables/views/list-variables.client.view.html'
            }).
            state('createVariable', {
                url: '/variables/create',
                templateUrl: 'modules/variables/views/create-variable.client.view.html'
            }).
            state('viewVariable', {
                url: '/variables/:variableId',
                templateUrl: 'modules/variables/views/view-variable.client.view.html'
            }).
            state('editVariable', {
                url: '/variables/:variableId/edit',
                templateUrl: 'modules/variables/views/edit-variable.client.view.html'
            });
    }
]);
