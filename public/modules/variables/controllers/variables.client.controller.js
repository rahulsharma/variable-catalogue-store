'use strict';

angular.module('variables').controller('VariablesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Variables', '$mdSidenav', '$http',
    function($scope, $stateParams, $location, Authentication, Variables, $mdSidenav, $http) {
        $scope.authentication = Authentication;
        //instantiate the suggestions
        $scope.suggestions = [];
        //instantiate selectedTags
        $scope.selectedTags = [];
        //instantiate selectedIndex
        $scope.selectedIndex = -1;
        //instantiate textBox model namespace
        $scope.textBox = {};

        //search related functions
        $scope.search = function () {
            $http.get('searchResults/' + $scope.textBox.searchText).success(function (data) {
                var resultsArr = [];
                data.forEach(function (x) {
                    resultsArr.push(x.elementName);
                });

                if ($scope.textBox.searchText.length > 0) {
                    $scope.suggestions = data;
                } else {
                    $scope.suggestions = [];
                }
                $scope.selectedIndex = -1;
            });
        };

        $scope.addToSelectedTags = function (index) {
            if ($scope.selectedTags.indexOf($scope.suggestions[index]) === -1) {
                $scope.selectedTags.push($scope.suggestions[index]);
                $scope.textBox.searchText = '';
                $scope.suggestions = [];
            }
        };

        $scope.checkKeyDown = function (event) {
            if (event.keyCode === 40) {
                event.preventDefault();
                if ($scope.selectedIndex + 1 !== $scope.suggestions.length) {
                    $scope.selectedIndex++;
                }
            } else if (event.keyCode === 38) {
                event.preventDefault();
                if ($scope.selectedIndex - 1 !== -1) {
                    $scope.selectedIndex--;
                }
            } else if (event.keyCode === 13) {
                $scope.addToSelectedTags($scope.selectedIndex);
            }
        };

        $scope.$watch('selectedIndex', function (val) {
            if (val !== -1) {
                $scope.textBox.searchText = $scope.suggestions[$scope.selectedIndex];
            }
        });

        $scope.toggleRight = function () {
            $mdSidenav('right').toggle();
        };

        $scope.close = function () {
            $mdSidenav('right').close();
        };

        $scope.removeTag = function (index) {
            $scope.selectedTags.splice(index, 1);
        };

        $scope.showRow = function(index){
            var suggestionItemRow = $scope.suggestions[index];
            suggestionItemRow.rowClicked = !suggestionItemRow.rowClicked;
        };
        //end of search related functions

        $scope.create = function() {
            var variable = new Variables({
                title: this.title,
                content: this.content
            });
            variable.$save(function(response) {
                $location.path('variables/' + response._id);

                $scope.title = '';
                $scope.content = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.remove = function(variable) {
            if (variable) {
                variable.$remove();

                for (var i in $scope.variables) {
                    if ($scope.variables[i] === variable) {
                        $scope.variables.splice(i, 1);
                    }
                }
            } else {
                $scope.variable.$remove(function() {
                    $location.path('variables');
                });
            }
        };

        $scope.update = function() {
            var variable = $scope.variable;

            variable.$update(function() {
                $location.path('variables/' + variable._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function() {
            $scope.variables = Variables.query();
        };

        $scope.findOne = function() {
            $scope.variable = Variables.get({
                variableId: $stateParams.variableId
            });
        };
    }
]);
