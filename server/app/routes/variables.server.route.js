'use strict';

/**
 * Module dependencies.
 */
var users = require('../controllers/users.server.controller.js'),
    variables = require('../controllers/variables.server.controller.js');

module.exports = function(app) {
    // Variables Routes
    app.route('/variables')
        .get(variables.list)
        .post(users.requiresLogin, variables.create);

    app.route('/variables/:variableId')
        .get(variables.read)
        .put(users.requiresLogin, variables.hasAuthorization, variables.update)
        .delete(users.requiresLogin, variables.hasAuthorization, variables.delete);

    // Finish by binding the variable middleware
    app.param('variableId', variables.variableByID);
};
