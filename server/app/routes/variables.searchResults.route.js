'use strict';

/**
 * Module dependencies.
 */
var users = require('../controllers/users.server.controller.js'),
    variablesSearchResults = require('../controllers/variables.searchResults.server.controller.js');

module.exports = function(app) {
    // Variables Routes
    app.route('/searchResults/:searchTerm')
        .get(users.requiresLogin,variablesSearchResults.searchByTerm);

    // Finish by binding the variable middleware
    app.param('searchTerm', variablesSearchResults.searchByTerm);
};


