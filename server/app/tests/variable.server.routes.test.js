'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Variable = mongoose.model('Variable'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, variable;

/**
 * Variable routes tests
 */
describe('Variable CRUD tests', function() {
    beforeEach(function(done) {
        // Create user credentials
        credentials = {
            username: 'username',
            password: 'password'
        };

        // Create a new user
        user = new User({
            firstName: 'Full',
            lastName: 'Name',
            displayName: 'Full Name',
            email: 'test@test.com',
            username: credentials.username,
            password: credentials.password,
            provider: 'local'
        });

        // Save a user to the test db and create new variable
        user.save(function() {
            variable = {
                title: 'Variable Title',
                content: 'Variable Content'
            };

            done();
        });
    });

    it('should be able to save an variable if logged in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new variable
                agent.post('/variables')
                    .send(variable)
                    .expect(200)
                    .end(function(variableSaveErr, variableSaveRes) {
                        // Handle variable save error
                        if (variableSaveErr) done(variableSaveErr);

                        // Get a list of variables
                        agent.get('/variables')
                            .end(function(variablesGetErr, variablesGetRes) {
                                // Handle variable save error
                                if (variablesGetErr) done(variablesGetErr);

                                // Get variables list
                                var variables = variablesGetRes.body;

                                // Set assertions
                                (variables[0].user._id).should.equal(userId);
                                (variables[0].title).should.match('Variable Title');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to save an variable if not logged in', function(done) {
        agent.post('/variables')
            .send(variable)
            .expect(401)
            .end(function(variableSaveErr, variableSaveRes) {
                // Call the assertion callback
                done(variableSaveErr);
            });
    });

    it('should not be able to save an variable if no title is provided', function(done) {
        // Invalidate title field
        variable.title = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new variable
                agent.post('/variables')
                    .send(variable)
                    .expect(400)
                    .end(function(variableSaveErr, variableSaveRes) {
                        // Set message assertion
                        (variableSaveRes.body.message).should.match('Title cannot be blank');

                        // Handle variable save error
                        done(variableSaveErr);
                    });
            });
    });

    it('should be able to update an variable if signed in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new variable
                agent.post('/variables')
                    .send(variable)
                    .expect(200)
                    .end(function(variableSaveErr, variableSaveRes) {
                        // Handle variable save error
                        if (variableSaveErr) done(variableSaveErr);

                        // Update variable title
                        variable.title = 'WHY YOU GOTTA BE SO MEAN?';

                        // Update an existing variable
                        agent.put('/variables/' + variableSaveRes.body._id)
                            .send(variable)
                            .expect(200)
                            .end(function(variableUpdateErr, variableUpdateRes) {
                                // Handle variable update error
                                if (variableUpdateErr) done(variableUpdateErr);

                                // Set assertions
                                (variableUpdateRes.body._id).should.equal(variableUpdateRes.body._id);
                                (variableUpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to get a list of variables if not signed in', function(done) {
        // Create new variable model instance
        var variableObj = new Variable(variable);

        // Save the variable
        variableObj.save(function() {
            // Request variables
            request(app).get('/variables')
                .end(function(req, res) {
                    // Set assertion
                    res.body.should.be.an.Array.with.lengthOf(1);

                    // Call the assertion callback
                    done();
                });

        });
    });


    it('should be able to get a single variable if not signed in', function(done) {
        // Create new variable model instance
        var variableObj = new Variable(variable);

        // Save the variable
        variableObj.save(function() {
            request(app).get('/variables/' + variableObj._id)
                .end(function(req, res) {
                    // Set assertion
                    res.body.should.be.an.Object.with.property('title', variable.title);

                    // Call the assertion callback
                    done();
                });
        });
    });

    it('should be able to delete an variable if signed in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new variable
                agent.post('/variables')
                    .send(variable)
                    .expect(200)
                    .end(function(variableSaveErr, variableSaveRes) {
                        // Handle variable save error
                        if (variableSaveErr) done(variableSaveErr);

                        // Delete an existing variable
                        agent.delete('/variables/' + variableSaveRes.body._id)
                            .send(variable)
                            .expect(200)
                            .end(function(variableDeleteErr, variableDeleteRes) {
                                // Handle variable error error
                                if (variableDeleteErr) done(variableDeleteErr);

                                // Set assertions
                                (variableDeleteRes.body._id).should.equal(variableDeleteRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete an variable if not signed in', function(done) {
        // Set variable user
        variable.user = user;

        // Create new variable model instance
        var variableObj = new Variable(variable);

        // Save the variable
        variableObj.save(function() {
            // Try deleting variable
            request(app).delete('/variables/' + variableObj._id)
                .expect(401)
                .end(function(variableDeleteErr, variableDeleteRes) {
                    // Set message assertion
                    (variableDeleteRes.body.message).should.match('User is not logged in');

                    // Handle variable error error
                    done(variableDeleteErr);
                });

        });
    });

    afterEach(function(done) {
        User.remove().exec();
        Variable.remove().exec();
        done();
    });
});
