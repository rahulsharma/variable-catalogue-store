'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * SearchResults Schema
 */
var SearchResultsSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId
    },
    elementName: {
        type: String,
        default: '',
        trim: true,
        required: 'ElementName cannot be blank'
    },
    settings: {
        grain_list:{
            type: Array
        },
        CreatedBy: {
            type: String,
            default: ''
        },
        CreatedDateTime: {
            type: Date,
            default: Date.now
        },
        PublishName: {
            type: String,
            default: ''
        },
        PublishDescription: {
            type: String,
            default: ''
        },
        Tags: {
            type: String,
            default: ''
        },
        filePath: {
            type: String,
            default: ''
        },
        script:{
            type: String,
            default: ''
        }
    }
}, { collection: 'element_catalogue' });

mongoose.model('SearchResults', SearchResultsSchema);
