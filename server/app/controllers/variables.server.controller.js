'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller.js'),
    Variable = mongoose.model('Variable'),
    _ = require('lodash');

/**
 * Create a variable
 */
exports.create = function(req, res) {
    var variable = new Variable(req.body);
    variable.user = req.user;

    variable.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(variable);
        }
    });
};

/**
 * Show the current variable
 */
exports.read = function(req, res) {
    console.log('are we here?');
    res.json(req.variable);
};

/**
 * Update a variable
 */
exports.update = function(req, res) {
    var variable = req.variable;

    variable = _.extend(variable, req.body);

    variable.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(variable);
        }
    });
};

/**
 * Delete an variable
 */
exports.delete = function(req, res) {
    var variable = req.variable;

    variable.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(variable);
        }
    });
};

/**
 * List of Variables
 */
exports.list = function(req, res) {
    Variable.find().sort('-created').populate('user', 'displayName').exec(function(err, variables) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(variables);
        }
    });
};

/**
 * Variable middleware
 */
exports.variableByID = function(req, res, next, id) {
    Variable.findById(id).populate('user', 'displayName').exec(function(err, variable) {
        if (err) return next(err);
        if (!variable) return next(new Error('Failed to load variable ' + id));
        req.variable = variable;
        next();
    });
};

/**
 * Variable authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (req.variable.user.id !== req.user.id) {
        return res.status(403).send({
            message: 'User is not authorized'
        });
    }
    next();
};
