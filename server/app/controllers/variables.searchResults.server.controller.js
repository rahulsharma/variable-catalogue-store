'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller.js'),
    SearchResults = mongoose.model('SearchResults'),
    _ = require('lodash');

/**
 * List of Search Results
 */
exports.searchByTerm = function(req, res, next, searchTerm) {
    var results = [];
    if(searchTerm.length > 1){
        SearchResults.find({"elementName":{$regex : searchTerm, $options:'i'}}).exec(function(err, searchResults) {

            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.json(searchResults);
            }
        });
    }
    else{
        res.json(results);
    }
};
